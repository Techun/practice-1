public class Main {
    public static void main(String[] args) {
        BusinessTrip[] trips = new BusinessTrip[5];

        trips[0] = new BusinessTrip("John Smith", 50.0, 3);
        trips[1] = new BusinessTrip("Doe Doe", 70.5, 4);
        trips[2] = null;
        trips[3] = new BusinessTrip("Bob Bobson", 25.2, 2);
        trips[4] = new BusinessTrip();

        // Выводим информацию о каждом объекте массива и находим путешествие с наибольшей стоимостью
        double maxCost = 0.0;
        BusinessTrip maxTrip = null;
        for (BusinessTrip trip : trips) {
            if (trip != null) {
                trip.show(); // Выводим информацию о путешествии
                if (trip.getTotal() > maxCost) {
                    maxCost = trip.getTotal();
                    maxTrip = trip;
                }
            }
        }
        assert maxTrip != null;
        System.out.println("Max cost: " + maxTrip.getAccount() + " (" + maxCost + ")"); // Выводим информацию о путешествии с максимальной стоимостью

        // изменяем затраты на транспорт последнего
        trips[4].setTransport(80.0);

        //общая длительность первых двух путешествий
        int totalDuration = trips[0].getDays() + trips[1].getDays();
        System.out.println("Duration: " + totalDuration); // Выводим общую длительность первых двух путешествий

        // выводим информацию о каждом объекте используя метод toString()
        for (BusinessTrip trip : trips) {
            if (trip != null) {
                System.out.println(trip);
            }
        }
    }
}

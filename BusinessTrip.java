public class BusinessTrip {
    private static final int rate = 23;  // ставка ежедневных расходов

    private String account;  // имя сотрудника
    private double transport;  // расходы на транспорт
    private int days;  // количество дней в командировке

    public BusinessTrip() {
        // Конструктор без параметров
    }

    public BusinessTrip(String account, double transport, int days) {
        this.account = account;
        this.transport = transport;
        this.days = days;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public double getTransport() {
        return transport;
    }

    public void setTransport(double transport) {
        this.transport = transport;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public double getTotal() {
        return transport + rate * days;  // общая стоимость поездки
    }

    public void show() {
        System.out.println("account=" + account);
        System.out.println("transport=" + transport);
        System.out.println("days=" + days);
        System.out.println("total=" + getTotal());
        System.out.print("\n");
    }

    public String toString() {
        return account + ";" + transport + ";" + days + ";" + getTotal();
    }
}